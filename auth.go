package main

import (
	"encoding/json"
	"fmt"
	"net/http"
	"strings"

	"github.com/golang-jwt/jwt"
	"gopkg.in/macaron.v1"
)

type KeycloakClaims struct {
	/*AuthTime       int      `json:"auth_time"`
	Typ            string   `json:"typ"`
	Azp            string   `json:"azp"`
	Nonce          string   `json:"nonce"`
	SessionState   string   `json:"session_state"`
	Acr            string   `json:"acr"`*/
	AllowedOrigins []string `json:"allowed-origins"`
	RealmAccess    struct {
		Roles []string `json:"roles"`
	} `json:"realm_access"`
	ResourceAccess struct {
		AdhesionAPI struct {
			Roles []string `json:"roles"`
		} `json:"adhesion-api"`
	} `json:"resource_access"`
	Scope             string   `json:"scope"`
	EmailVerified     bool     `json:"email_verified,omitempty"`
	Name              string   `json:"name"`
	PreferredUsername string   `json:"preferred_username,omitempty"`
	GivenName         string   `json:"given_name,omitempty"`
	Locale            string   `json:"locale,omitempty"`
	FamilyName        string   `json:"family_name,omitempty"`
	Email             string   `json:"email"`
	AdhesionUserID    int      `json:"adhesion_user_id,omitempty"`
	Audience          []string `json:"aud,omitempty"`
	jwt.StandardClaims
}

func (claims KeycloakClaims) PerformKeycloakChecks() bool {
	for _, aud := range claims.Audience {
		if aud == audience {
			return claims.Issuer == KeycloakRealmURL
		}
	}
	return false
}
func (claims KeycloakClaims) HasRole(role string) bool {
	for _, claim_role := range claims.ResourceAccess.AdhesionAPI.Roles {
		if claim_role == role {
			return true
		}
	}
	return false
}

func InitAuth() {
	res, err := http.Get(KeycloakRealmURL)
	if err != nil {
		println("impossible de se connecter à Keycloak !")
		he(err)
	}
	dec := json.NewDecoder(res.Body)
	data := map[string]string{}
	dec.Decode(&data)
	//fmt.Printf("%v",data)

	publickey, er := jwt.ParseRSAPublicKeyFromPEM([]byte("-----BEGIN CERTIFICATE-----\n" + data["public_key"] + "\n-----END CERTIFICATE-----"))
	if er != nil {
		fmt.Println(er)
		return
	}
	PublicKey = publickey

	//fmt.Printf("pk: %v",PublicKey)
	if PublicKey == nil {
		panic("could not get Keycloak public Key")
	}
	if PublicKey.Size() == 0 {
		panic("could not get Keycloak public Key")
	}
}

///func a(ctx *macaron.Context){
/*
Utilisé par l'authentification HTTP et Websocket (1er message envoyé par le client  = jwt)
*/
func ProcessToken(reqToken string) (valid bool, claims *KeycloakClaims, err error) {
	var _claim KeycloakClaims
	claims = &_claim
	token, err := jwt.ParseWithClaims(reqToken, claims, func(token *jwt.Token) (interface{}, error) {
		// Don't forget to validate the alg is what you expect:
		if _, ok := token.Method.(*jwt.SigningMethodRSA); !ok {
			return nil, fmt.Errorf("unexpected signing method: %v", token.Header["alg"])
		}
		if token.Header["alg"] != "RS256" {
			return nil, fmt.Errorf("bad algorithm: %v", token.Header["alg"])
		}
		return PublicKey, nil
	})
	if err != nil {
		return false, claims, err
	}

	//fmt.Printf("\nclaims:%v\n", claims)
	//fmt.Printf("name: %s\nroles : %v\n", claims.Name, claims.ResourceAccess.AdhesionAPI.Roles)
	return token.Valid && claims.PerformKeycloakChecks(), claims, nil
}

func AuthenticationMiddleware(ctx *macaron.Context) {
	auth := ctx.Req.Header.Get("Authorization")
	if auth != "" {
		parts := strings.Split(auth, " ")
		if len(parts) != 2 {
			ctx.JSON(401, ErrorDetail{Detail: "Invalid Authorization header (length)"})
			return
		}
		if parts[0] != "Bearer" {
			ctx.JSON(401, ErrorDetail{Detail: "Invalid Authorization header (identifier)"})
			return
		}
		token := parts[1]
		valid, claims, err := ProcessToken(token)
		if err != nil {
			ctx.JSON(401, ErrorDetail{Detail: err.Error()})
			return
		}
		if !valid {
			ctx.JSON(401, ErrorDetail{Detail: "token verification failed"})
			return
		}
		ctx.Map(claims)
		ctx.Next()
		return
	} else {
		ctx.JSON(401, ErrorDetail{Detail: "Login is required"})
	}
}

func RBACMiddleWare(required_role string) func(*macaron.Context, *KeycloakClaims) {
	return func(ctx *macaron.Context, claims *KeycloakClaims) {
		if claims.HasRole(required_role) {
			ctx.Next()
			return
		} else {
			ctx.JSON(403, ErrorDetail{Detail: fmt.Sprintf("Vous n'avez pas la permission (rôle %s)", WatcherRole)})
		}
	}
}
