package main

import "gopkg.in/macaron.v1"

type WebConfig struct {
	AdminFrontendEnabled  bool   `json:"admin_frontend_enabled"`
	PublicFrontendEnabled bool   `json:"public_frontend_enabled"`
	Message               string `json:"message"`
}

type AdhesionConfig struct {
	Registration1aEnabled  bool `json:"registration_1a_enabled"`
	Registration2apEnabled bool `json:"registration_2ap_enabled"`
}

var GlobalWebConfig = &WebConfig{
	AdminFrontendEnabled:  true,
	PublicFrontendEnabled: true,
	Message:               "le serveur est en maintenance",
}

func EditWebConfig(newcfg WebConfig, ctx *macaron.Context) {
	if GlobalWebConfig.AdminFrontendEnabled != newcfg.AdminFrontendEnabled {
		BroadcastLoggers(LogEvent{
			Type: EventMaintenance,
			Data: !newcfg.AdminFrontendEnabled, //maintenance
		})
	}
	if GlobalWebConfig.PublicFrontendEnabled != newcfg.PublicFrontendEnabled {
		BroadcastCustomers(LogEvent{
			Type: EventMaintenance,
			Data: !newcfg.PublicFrontendEnabled,
		})
	}
	GlobalWebConfig = &newcfg
	ctx.JSON(200, GlobalWebConfig)
}
func RetrieveWebConfig(ctx *macaron.Context) {
	ctx.JSON(200, GlobalWebConfig)
}
