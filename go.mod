module cdpwatcher

go 1.16

require (
	github.com/dchest/uniuri v0.0.0-20200228104902-7aecb25e1fe5
	github.com/go-macaron/binding v1.1.1 // indirect
	github.com/go-macaron/cors v0.0.0-20190925001837-b0274f40d4c7
	github.com/go-macaron/inject v0.0.0-20200308113650-138e5925c53b // indirect
	github.com/golang-jwt/jwt v3.2.1+incompatible
	github.com/gorilla/websocket v1.4.2
	gopkg.in/macaron.v1 v1.3.5
)
