# sidecar management
Ce serveur permet de gérer un mode maintenance : on peut désactiver à distance
les frontends pour empêcher les utilisateurs de rencontrer des erreurs.
Le mode maintenance est appliqué non seulement via un endpoint mais
aussi via des websockets, donc c'est instantané (pour réactiver aussi).

Toutes les actions demandent des permissions, et ce serveur reçoit les JWT de
Keycloak et les décode+vérifie, il n'a donc pas besoin de credentials

Les websockets doublent également pour un suivi temps réel des actions (pas implémenté)
et pour un envoi de messages.

pour l'instant tout est fait uniquement sur le frontend admin

## Kubernetes

Il faut mettre ce conteneur dans un Pod séparé de celui d'adhésion, sinon ça ne sert à rien pour le mode maintenance.

## sécurité

L'authentification est faite par la vérification de tokens JWT. La clé publique est téléchargée depuis Keycloak au
lancement.