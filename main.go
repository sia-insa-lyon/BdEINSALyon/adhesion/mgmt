package main

import (
	"math/rand"
	"net/http"
	"time"

	"github.com/go-macaron/binding"
	"github.com/go-macaron/cors"
	"gopkg.in/macaron.v1"
)

//var listen_addr = flag.String("listen-addr", ":8090", "address to listen on, ip:port")

func WatcherEndpoint(w http.ResponseWriter, r *http.Request) {
	handleWs(w, r, watcher)
}

func LoggerEndpoint(w http.ResponseWriter, r *http.Request) {
	//rb:=bufio.NewReader(r.Body)
	//rb.ReadString('\n')
	handleWs(w, r, cdplogger)
}

func StatsEndpoint(ctx *macaron.Context, claims *KeycloakClaims) {
	//println(claims.Name)
	bufl := make([]string, loggers.Len())
	i := 0
	for e := loggers.Front(); e != nil; e = e.Next() {
		state := e.Value.(CdpState)
		bufl[i] = state.username
		i += 1
	}
	ctx.JSON(200, map[string]interface{}{
		"loggers":         bufl,
		"customers_count": CustomersCount,
	})
}

func main() {
	InitAuth()
	rand.Seed(time.Now().UnixNano())

	WatcherPermission := RBACMiddleWare(WatcherRole)
	ManagementPermission := RBACMiddleWare(ManagerRole)

	corsLocked := cors.CORS(cors.Options{
		Scheme:           "http",
		AllowDomain:      FrontendDomain,
		AllowSubdomain:   true,
		Methods:          []string{"GET", "POST", "OPTIONS", "HEAD"},
		AllowCredentials: true,
	})
	openCors := cors.CORS(cors.Options{AllowDomain: "*"})

	m := macaron.New()
	m.Use(macaron.Renderer(macaron.RenderOptions{IndentJSON: true}))
	//m.Use(macaron.Static("static"))
	m.Use(macaron.Recovery())
	m.Use(macaron.Logger())

	m.Group("/protected", func() { // partie "sensible"
		m.Use(corsLocked)
		m.Get("/stats", WatcherPermission, StatsEndpoint)
		m.Post("/broadcast", ManagementPermission, binding.Bind(LogEvent{}), BroadcastLoggers)
		m.Post("/brd_customers", ManagementPermission, binding.Bind(LogEvent{}), BroadcastCustomers)
		m.Post("/webconfig", ManagementPermission, binding.Bind(WebConfig{}), EditWebConfig)
		m.Get("/broadcast", func() string { return "lol" })
	}, AuthenticationMiddleware)

	m.Get("/webconfig", openCors, RetrieveWebConfig)

	m.Get("/logger", openCors, LoggerEndpoint) // websocket, pas de permissions en HTTP
	m.Get("/watch", openCors, WatcherEndpoint) // websocket, pas de permission en HTTP
	m.Get("/customer_brd", openCors, handleCustomer)

	go watchloop() // distribue les messages

	m.Run("0.0.0.0", int(port))
	//he(http.ListenAndServe(*listen_addr, m))
}

func he(err error) {
	if err != nil {
		panic(err)
	}
}

func phe(err error) {
	if err != nil {
		println(err)
	}
}
