package main

import (
	"container/list"
	"crypto/rsa"
	"fmt"
	"os"
	"strconv"

	"github.com/gorilla/websocket"
)

type CdpState struct {
	conn     *websocket.Conn //si nil, pas connecté
	username string
}
type ErrorDetail struct {
	Detail string `json:"detail"`
	Code   int    `json:"code"`
}

var watchers *list.List  //websocket.Conn
var loggers *list.List   //CdpState
var customers *list.List //websocket.Conn

var LoggerSequence int = 0

var PublicKey *rsa.PublicKey = nil
var audience string
var KeycloakRealmURL string
var port int64
var WatcherRole string
var ManagerRole string
var FrontendPort string
var FrontendDomain string
var Scheme string
var CustomersCount int = 0

var PublicFrontendPort string

func init() {
	watchers = list.New()
	loggers = list.New()
	customers = list.New()
	kgb_kun = make(chan []byte)
	customer_brd = make(chan []byte)

	audience = Getenv("AUDIENCE", "adhesion-api")
	KeycloakRealmURL = Getenv("KEYCLOAK_URL", "http://localhost:8080/auth/realms/asso-insa-lyon")
	WatcherRole = Getenv("WATCHER_ROLE", "admin")
	ManagerRole = Getenv("MANAGER_ROLE", "mgmt")
	Scheme = Getenv("SCHEME", "http")
	FrontendDomain = Getenv("FRONTEND_DOMAIN", "localhost")
	FrontendPort = Scheme + "://" + FrontendDomain + Getenv("FRONTEND_PORT", ":4200")
	PublicFrontendPort = Getenv("PUBLIC_FRONTEND_URL", "http://localhost:3000")

	println("values:")
	fmt.Printf("keycloak: %s\nfrontendp:%s\npublicfront: %s", KeycloakRealmURL, FrontendPort, PublicFrontendPort)

	var perr error
	port, perr = strconv.ParseInt(Getenv("PORT", "8090"), 10, 16)
	if perr != nil {
		port = 8090
	}
}

func Getenv(key string, fallback string) string {
	value, set := os.LookupEnv(key)
	if set {
		return value
	} else {
		return fallback
	}
}
