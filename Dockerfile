FROM golang:1.16 as builder

RUN mkdir -p /go/src/mgmt
WORKDIR /go/src/mgmt
ENV GOPATH /go
COPY go.mod .
COPY go.sum .
# pour forcer Go sans donner le code source
COPY dummy.go .
# téléchargement des dépendances
RUN go mod tidy

COPY *.go /go/src/mgmt/
# pour les oublis
RUN go mod tidy

# build Go
RUN CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -o main

#FROM alpine:3.11
#COPY --from=builder /go/src/mgmt/main .

EXPOSE 8090
ENV PORT 8090
ENV MANAGER_ROLE "mgmt"
ENV WATCHER_ROLE "watcher"
ENV KEYCLOAK_URL "http://localhost:8080/auth/realms/asso-insa-lyon"
ENV AUDIENCE "adhesion-api"
ENV MACARON_ENV "production"
ENV FRONTEND_DOMAIN "localhost"
ENV SCHEME "http"
ENV FRONTEND_PORT ":4200"
ENV PUBLIC_FRONTEND_URL "http://localhost:3000"
# mettre "" pour 80 / 443
CMD ["/go/src/mgmt/main"]