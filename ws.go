package main

import (
	"encoding/json"
	"fmt"
	"net/http"
	"strings"

	"github.com/gorilla/websocket"
)

type EventType string

const EventConnect EventType = "connected"
const EventDisconnect EventType = "disconnected"
const EventInfo EventType = "info"
const EventError EventType = "error"

const EventChatDialog EventType = "dialog"
const EventChatSnackbar EventType = "snackbar"

const EventMaintenance EventType = "maintenance"

var kgb_kun chan []byte
var customer_brd chan []byte

type LogEvent struct {
	Type EventType   `json:"type" form:"type"`
	Data interface{} `json:"data" form:"type"`
}
type WatchEvent struct {
	User string `json:"user,omitempty"`
	LogEvent
}

var httpToWs = websocket.Upgrader{
	CheckOrigin: func(r *http.Request) bool {
		origin := r.Header.Get("Origin")
		fmt.Printf("url: %s\n", origin)
		return origin == FrontendPort || origin == PublicFrontendPort
	},
}

func cdplogger(conn *websocket.Conn, claims *KeycloakClaims) {
	state := CdpState{
		conn:     conn,
		username: fmt.Sprintf("%s#%d", claims.Name, LoggerSequence),
	}
	LoggerSequence += 1
	elem := loggers.PushBack(state)
	con_ev, cerr := json.Marshal(WatchEvent{
		User: state.username,
		LogEvent: LogEvent{
			Data: state.username,
			Type: EventConnect,
		},
	})
	if cerr == nil {
		kgb_kun <- con_ev
	}
	for {
		_, msg, err := conn.ReadMessage()
		if err != nil {
			break
		}
		fmt.Println("rcv: ", string(msg))
		event := WatchEvent{}
		if err := json.Unmarshal(msg, &event); err != nil {
			fmt.Println("erreur decodage,", err.Error())
			kgb_kun <- msg // copie directe
		} else {
			event.User = state.username
			fmt.Println("event decoded:", event.Type)
			if tosend, jerr := json.Marshal(&event); jerr == nil {
				kgb_kun <- tosend
			}
		}
	}
	loggers.Remove(elem)
	disc_ev, derr := json.Marshal(WatchEvent{
		User: claims.Name,
		LogEvent: LogEvent{
			Data: state.username,
			Type: EventDisconnect,
		},
	})
	if derr == nil {
		kgb_kun <- disc_ev
	}
}

func watchloop() {
	for {
		msg := <-kgb_kun
		fmt.Println("forwarding", string(msg))
		for e := watchers.Front(); e != nil; e = e.Next() {
			conn := e.Value.(*websocket.Conn)
			phe(conn.WriteMessage(websocket.TextMessage, msg))
		}
	}
}
func watcher(conn *websocket.Conn, claims *KeycloakClaims) {
	if !claims.HasRole(WatcherRole) {
		conn.WriteJSON(LogEvent{
			Type: EventError,
			Data: 401,
		})
		conn.Close()
		return
	}
	elem := watchers.PushBack(conn)
	for {
		_, msg, err := conn.ReadMessage()
		if err != nil {
			fmt.Printf("watcher, %v", err)
			break
		}
		println(msg)
	}
	watchers.Remove(elem)
}

func handleWs(w http.ResponseWriter, r *http.Request, handler func(conn *websocket.Conn, claims *KeycloakClaims)) {
	conn, err := httpToWs.Upgrade(w, r, nil)
	he(err)
	fmt.Printf("client %s connected\n", conn.RemoteAddr().String())
	_, m, _ := conn.ReadMessage()
	msg := strings.Trim(string(m), "\n\"")
	ok, claims, cerr := ProcessToken(string(msg))
	if cerr != nil {
		//conn.WriteMessage(websocket.TextMessage, []byte(cerr.Error()))
		conn.WriteJSON(LogEvent{
			Type: EventError,
			Data: cerr.Error(),
		})
		conn.Close()
		return
	}
	if !ok {
		conn.WriteJSON(LogEvent{
			Type: EventError,
			Data: 401,
		})
		//conn.WriteMessage(websocket.TextMessage, []byte("erreur d'authentification"))
		conn.Close()
		return
	}
	conn.WriteJSON(LogEvent{
		Type: EventInfo,
		Data: "Succès",
	})
	handler(conn, claims)
}

func BroadcastLoggers(msg LogEvent) {
	for e := loggers.Front(); e != nil; e = e.Next() {
		state := e.Value.(CdpState)
		state.conn.WriteJSON(&msg)
	}
}

func BroadcastCustomers(msg LogEvent) {
	for e := customers.Front(); e != nil; e = e.Next() {
		conn := e.Value.(*websocket.Conn)
		he(conn.WriteJSON(msg))
	}
}

func handleCustomer(w http.ResponseWriter, r *http.Request) {
	conn, err := httpToWs.Upgrade(w, r, nil)
	if err != nil {
		println(err.Error())
		return
	}
	println("new customer")
	elem := customers.PushBack(conn)
	he(conn.WriteJSON(LogEvent{
		Type: EventMaintenance,
		Data: !GlobalWebConfig.PublicFrontendEnabled,
	}))
	CustomersCount += 1
	for {
		if _, msg, err := conn.ReadMessage(); err != nil {
			println(err.Error())
			break
		} else {
			println("customer msg", msg)
		}
	}
	customers.Remove(elem)
	CustomersCount -= 1
	conn.Close()
	println("end customer")
}
